package model;
import java.util.Random;

public class Rechteck {
	private static Random r = new Random();
	private Punkt p;
	private int x;
	private int y;
	private int breite;
	private int hoehe;
	public static final int maxbereite = 1200;
	public static final int maxhoehe = 1000;

	public Rechteck() {
		this.p = new Punkt();
		this.breite = 0;
		this.hoehe = 0;

	}

	public Rechteck(int x, int y, int breite, int hoehe) {
		super();
		this.x = x;
		this.y = y;
		this.breite = breite;
		this.hoehe = hoehe;
		this.p = new Punkt(x,y);
	}

	public int getX() {
		return this.p.getX();
	}

	public void setX(int x) {
		this.p.getY();
	}

	public int getY() {
		return this.p.getY();
	}

	public void setY(int y) {
		this.p.getY();
	}

	public int getBreite() {
		return breite;
	}

	public void setBreite(int breite) {
		this.breite =Math.abs(breite);
	}

	public int getHoehe() {
		return hoehe ;
	}

	public int setHoehe(int hoehe) {
		this.hoehe = Math.abs(hoehe);
		return hoehe;
	}

	@Override
	public String toString() {
		return "Rechteck [x=" + x + ", y=" + y + ", breite=" + breite + ", hoehe=" + hoehe + "]";
	}
	 public boolean enthaelt(int x, int y) {
		return this.p.getX() <= x && x <= this.p.getX() + this.breite &&
				this.p.getY() <= y && y <= this.p.getY() + this.hoehe;
		 
	 }
	 public boolean enthaelt(Punkt p) {
			return enthaelt(p.getX(), p.getY());
		 
	 }
	 public boolean enthaelt(Rechteck rechteck) {
		 Punkt left = new Punkt(rechteck.getX(), rechteck.getY());
			Punkt right = new Punkt(rechteck.getX() + rechteck.getBreite(), rechteck.getY() + rechteck.getHoehe());
			return enthaelt(left) && enthaelt(right);
		 
	 }
	 
	 
	 public static Rechteck generiereZufallsRechteck(){
			int x  =r.nextInt(maxbereite +1);
			int y = r.nextInt(maxhoehe+1);
			int breite = r.nextInt(maxbereite -x+1);
			int hoehe = r.nextInt(maxhoehe-y+1);
			return new Rechteck(x, y, breite, hoehe);

}
}
