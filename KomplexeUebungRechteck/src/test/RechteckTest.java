package test;

import model.Rechteck;
import controller.BunteRechteckeController;

public class RechteckTest {

	public static void main(String[] args) {

		Rechteck rechteck0 = new Rechteck();
		rechteck0.setX(10);
		rechteck0.setY(10);
		rechteck0.setBreite(30);
		rechteck0.setHoehe(40);

		Rechteck rechteck1 = new Rechteck();
		rechteck1.setX(25);
		rechteck1.setY(25);
		rechteck1.setBreite(100);
		rechteck1.setHoehe(20);

		Rechteck rechteck2 = new Rechteck();
		rechteck2.setX(260);
		rechteck2.setY(10);
		rechteck2.setBreite(200);
		rechteck2.setHoehe(100);

		Rechteck rechteck3 = new Rechteck();
		rechteck3.setX(5);
		rechteck3.setY(500);
		rechteck3.setBreite(300);
		rechteck3.setHoehe(25);

		Rechteck rechteck4 = new Rechteck();
		rechteck4.setX(100);
		rechteck4.setY(100);
		rechteck4.setBreite(100);
		rechteck4.setHoehe(100);

		Rechteck rechteck5 = new Rechteck(200, 200, 200, 200);
		Rechteck rechteck6 = new Rechteck(800, 400, 20, 20);
		Rechteck rechteck7 = new Rechteck(800, 450, 20, 20);
		Rechteck rechteck8 = new Rechteck(850, 400, 20, 20);
		Rechteck rechteck9 = new Rechteck(855, 455, 25, 25);
		Rechteck eck10 = new Rechteck(-4, -5, -50, -200);
		System.out.println("eck10" + eck10);
		
		Rechteck eck11 = new Rechteck();
		eck11.setX(-10);
		eck11.setY(-10);
		eck11.setBreite( -200);
		eck11.setHoehe( -100 );
		System.out.println("eck11" +eck11);

		System.out.println(rechteck0.toString());
		System.out.println(rechteck0.toString().equals("Rechteck [x=10, y=10, breite=30, hoehe=40]"));

		BunteRechteckeController brc = new BunteRechteckeController();
		brc.add(rechteck0);
		brc.add(rechteck1);
		brc.add(rechteck2);
		brc.add(rechteck3);
		brc.add(rechteck4);
		brc.add(rechteck5);
		brc.add(rechteck6);
		brc.add(rechteck7);
		brc.add(rechteck8);
		brc.add(rechteck9);
		
		
		
		Rechteck eck12 = Rechteck.generiereZufallsRechteck();
		System.out.println("das ist Rechteck12"+eck12);

		System.out.println(brc.toString());
		
		rechteckeTesten();
		 
	}
	
	private static void rechteckeTesten() {
		boolean testen = true;
		Rechteck[] rechtecke = new Rechteck[50000];
		Rechteck referenz = new Rechteck(0,0,1200,1000);
		for(int i = 0; i < rechtecke.length; i++) {
			rechtecke[i] = Rechteck.generiereZufallsRechteck();
			if(!referenz.enthaelt(rechtecke[i])) {
				System.out.println(rechtecke[i]);
				testen = false;
			}
		}
		System.out.println("sie die Rechtecke erfolgreich gestalten: " + testen);
}
}