package controller;

import java.util.LinkedList;

import model.Rechteck;

public class BunteRechteckeController {

	private LinkedList<Rechteck> rechtecke;

	public BunteRechteckeController() {
		this.rechtecke = new LinkedList<Rechteck>();
	}

	public void add(Rechteck rechteck) {
		rechtecke.add(rechteck);
	}

	public void reset() {
		rechtecke.clear();
	}

	public LinkedList<Rechteck> getRechtecke() {
		return rechtecke;
	}

	@Override
	public String toString() {
		return "BunteRechteckeController [rechtecke=" + rechtecke + "]";
	}
	
	public void generiereZufallsRechtecke(int anzahl) {
		this.reset();
		for(int i = 0; i < anzahl; i++)
			rechtecke.add(Rechteck.generiereZufallsRechteck());
	}

	public static void main(String[] args) {

	}

}
