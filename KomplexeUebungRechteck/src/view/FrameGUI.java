package view;

import java.awt.event.ActionEvent;
import controller.BunteRechteckeController;
import java.awt.event.ActionListener;

import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JPanel;
import javax.swing.JMenuItem;

import javax.swing.border.EmptyBorder;



public class FrameGUI extends JFrame {

	private JPanel contentPane;
	private JMenuBar menuBar;
	private JMenu menu;
	private JMenuItem menuItemneuRechteck;
	private BunteRechteckeController brc;;

	public static void main(String[] args) {
		new FrameGUI().run();

	}

	protected void run() {
		while (true) {
			try {
				Thread.sleep(100);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			this.revalidate();
			this.repaint();
		}

	}

	public FrameGUI() {
		this.brc = new BunteRechteckeController();
		setTitle("Rechteckviewtest");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(0, 0, 1260, 1000);
		this.menuBar = new JMenuBar();
		this.setJMenuBar(menuBar);
		this.menu = new JMenu("hin�gen");
		this.menuBar.add(menu);
		this.menuItemneuRechteck = new JMenuItem("f�gen von Rechteck");
		this.menuItemneuRechteck.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				menuItemNeuesRechteck_Clicked();
			}

		});
		this.menu.add(this.menuItemneuRechteck);

		contentPane = new Zeichenflaeche(brc);
		contentPane.setBorder(new EmptyBorder(3, 3, 3, 3));
		setContentPane(contentPane);
		this.setVisible(true);
	}

	public void menuItemNeuesRechteck_Clicked() {
		new RechteckGUI(brc);

	}
}
