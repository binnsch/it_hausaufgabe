package view;

import controller.BunteRechteckeController;
import model.Rechteck;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;



public class RechteckGUI extends JFrame {
	private static final long version = 1L;
	private JPanel contentPane;
	private JTextField RX;
	private JTextField RY;
	private JTextField RLaenge;
	private JTextField RHoehe;
	private BunteRechteckeController brc;
	
	
	

	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					RechteckGUI frame = new RechteckGUI(new BunteRechteckeController());
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
	
	
	
	
	
	public RechteckGUI(BunteRechteckeController brc) {
		this.brc = brc;
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(new BorderLayout(0, 0));
		
		JPanel panel1 = new JPanel();
		contentPane.add(panel1, BorderLayout.CENTER);
		panel1.setLayout(new GridLayout(0, 1, 0, 0));
		
		JLabel labelX = new JLabel("X");
		panel1.add(labelX);
		
		RX = new JTextField();
		panel1.add(RX);
		RX.setColumns(10);
		
		JLabel labelY = new JLabel("Y");
		panel1.add(labelY);
		
		RY = new JTextField();
		panel1.add(RY);
		RY.setColumns(10);
		
		JLabel labellaeange = new JLabel("lange");
		panel1.add(labellaeange);
		
		RLaenge = new JTextField();
		panel1.add(RLaenge);
		RLaenge.setColumns(10);
		
		JLabel labelHohe = new JLabel("hohe");
		panel1.add(labelHohe);
		
		RHoehe = new JTextField();
		panel1.add(RHoehe);
		RHoehe.setColumns(10);
		
		JButton btnSpeichern = new JButton("speichern");
		panel1.add(btnSpeichern);
		btnSpeichern.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				speichenButten();
			}
		});
		setVisible(true);
	}
	
	protected void speichenButten() {
		int x = Integer.parseInt(RX.getText());
		int y = Integer.parseInt(RY.getText());
		int laenge = Integer.parseInt(RLaenge.getText());
		int hoehe = Integer.parseInt(RHoehe.getText());
		Rechteck r = new Rechteck(x,y, laenge, hoehe);
		brc.add(r);

	}
}
