package view;

import java.awt.Color;
import java.awt.Graphics;
import java.lang.ModuleLayer.Controller;

import javax.swing.JPanel;

import controller.BunteRechteckeController;

public class Zeichenflaeche extends JPanel {

	private BunteRechteckeController bc;

	public Zeichenflaeche(BunteRechteckeController controller) {
		this.bc = controller;

	}

	public void paintComponent(Graphics g) {
		int laenge = bc.getRechtecke().size();
		for (int i = 0; i < laenge; i++) {

			g.setColor(Color.red);
			g.drawRect(bc.getRechtecke().get(i).getX(), bc.getRechtecke().get(i).getY(),
					bc.getRechtecke().get(i).getBreite(), bc.getRechtecke().get(i).getHoehe());
		}
	}


}
